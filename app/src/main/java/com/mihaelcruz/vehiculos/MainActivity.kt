package com.mihaelcruz.vehiculos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.mensaje.*
import kotlinx.android.synthetic.main.mensaje.view.*
import kotlinx.android.synthetic.main.mensaje.view.tvMarca

class MainActivity : AppCompatActivity() {
    var nombreMensaje=""
    var marcaMensaje=""
    var anioMensaje=""
    var descripcionMensaje=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val marcas = listOf("Toyota", "Porsche", "Mercedes-Benz", "Ford", "Chevrolet", "Nissan", "Mitsubishi", "Cadillac")

        val adaptador = ArrayAdapter(this,R.layout.style_spinner_item, marcas)
        spMarcas.adapter = adaptador
        spMarcas.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?,p1: View?,position: Int,p3: Long) {
                marcaMensaje = marcas[position]
                Toast.makeText(applicationContext, marcas[position], Toast.LENGTH_SHORT).show()
            }
        }


        btnConfirmar.setOnClickListener{
            val obj:Vehiculo= Vehiculo(nombreMensaje,marcaMensaje,anioMensaje,descripcionMensaje)
            nombreMensaje = edtNombres.text.toString()
            anioMensaje = edtAnio.text.toString()
            descripcionMensaje = edtDescripcion.text.toString()
            crearDialogo(nombreMensaje,marcaMensaje,anioMensaje,descripcionMensaje).show()
        }
    }
    fun crearDialogo(nombreMensaje:String,marcaMensaje:String,anioMensaje:String,descripcionMensaje:String):AlertDialog{
        val alertDialog:AlertDialog
        val builder =AlertDialog.Builder(this)
        val vista:View = layoutInflater.inflate(R.layout.mensaje,null)
        builder.setView(vista)

        val btnCerrar:Button = vista.findViewById(R.id.btnCerrar)
        val tvNombre:TextView = vista.findViewById(R.id.tvNombre)
        val tvMarca:TextView = vista.findViewById(R.id.tvMarca)
        val tvAnio:TextView = vista.findViewById(R.id.tvAnio)
        val tvDescripcion:TextView = vista.findViewById(R.id.tvDescripcion)

        alertDialog = builder.create()
        tvNombre.text = "Nombre: $nombreMensaje"
        tvMarca.text = "Marca: $marcaMensaje"
        tvAnio.text = "Año: $anioMensaje"
        tvDescripcion.text = "Descripción: $descripcionMensaje"
        btnCerrar.setOnClickListener{
            alertDialog.dismiss()
        }


        return alertDialog
    }
}