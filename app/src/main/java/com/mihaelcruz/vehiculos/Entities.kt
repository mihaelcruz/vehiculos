package com.mihaelcruz.vehiculos

import java.lang.reflect.Constructor

data class Vehiculo (val nombresVehiculo:String,
                     val marcaVehiculo:String,
                     val anioVehiculo:String,
                     val descripcionVehiculo:String){

}